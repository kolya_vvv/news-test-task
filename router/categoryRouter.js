const express = require('express');

const categoryController = require('../controllers/categoryController');

const router = express.Router();

router.get('/all', categoryController.getAll);
router.get('/:id', categoryController.getOne);
router.post('/', categoryController.create);

module.exports = router;
