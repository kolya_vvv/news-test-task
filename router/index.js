const express = require('express');

const categoryRouter = require('./categoryRouter');
const newsRouter = require('./newsRouter');
const articleRouter = require('./articleRouter');

const router = express.Router();

router.use('/category', categoryRouter);
router.use('/news', newsRouter);
router.use('/article', articleRouter);


module.exports = router;
