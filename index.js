require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');

const router = require('./router');
const errorHandler = require('./middleware/errorHandlingMiddleware');

const PORT = process.env.PORT || 5000;
const DB_PATH = process.env.DB_PATH;

const app = express();

app.use('/api', router);

app.use((req, res, next) => {
    res.status(404).send('page not found');
});

app.use(errorHandler);

const start = async () => {
    try {
        await mongoose.connect(DB_PATH);

        app.listen(8888, () => {
            console.log(`server listen port ${8888}`)
        })
    } catch (error) {
        console.log(error);
    }
}

start();