module.exports = (err, req, res, next) => {
    console.log(err);

    if(err.status){
        return res.status(err.status).json({message: err.message})
    }else {
        return res.status(500).json({message: err.message})
    }   
}