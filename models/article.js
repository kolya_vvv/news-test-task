const mongoose = require('mongoose');

const Article = mongoose.model('Article', {
    title: String,
    content: String
});

module.exports = Article;
