const { faker } = require('@faker-js/faker');

const Article = require('../models/article');

class ArticleController {
    async createArticles (req, res, next) {
        try {
            for (let index = 0; index < 20; index++) {
                const articleData = {
                    title: faker.random.words(Math.floor(1 + Math.random() * 10)),
                    content: faker.random.words(200)
                }
    
                const article = new Article(articleData);
    
                await article.save();
            }

            res.status(200).json({massage: "articles created"});
        } catch (error) {
            next(error)
        }
    }
    
    async getOne (req, res, next) {
        try {
            const id = req.params.id;

            const article = await Article.findById(id);

            res.send(article);
        } catch (error) {
            next(error)
        }
    }
}

module.exports = new ArticleController();
